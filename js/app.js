async function getData() {
  try {
    const responseUsers = await fetch("https://ajax.test-danit.com/api/json/users");
    const dataUsers = await checkStatusResponse(responseUsers);
    const responsePosts = await fetch("https://ajax.test-danit.com/api/json/posts");
    const dataPosts = await checkStatusResponse(responsePosts);

    dataPosts.forEach(post => {
      const user = dataUsers.find(user => user.id === post.userId);
      if (user) {
        const card = new Card(post.title, post.body, user.name, user.email, post.id);
        card.addCard();
      }
    });
  } catch (err) {
    console.error(err);
  }
}
getData()

function checkStatusResponse(response) {
  if (response.ok) {
    return response.json();
  } else {
    throw new Error('Error');
  }
};
class Card {
  constructor(title, body, name, email, postId) {
    this.title = title,
      this.body = body,
      this.name = name,
      this.email = email,
      this.postId = postId
  }
  cardTemplate() {
    const card = `
    <div class="card" id="${this.postId}">
      <div class="user-info">
        <h2 class="user-name">${this.name}</h2>
        <span class="user-email">${this.email}</span>
      </div>
      <div class="user-post">
        <h3 class="post-title">${this.title}</h3>
        <p class="post-body">${this.body}</p>
      </div>
    </div>
    `
    return card;
  }
  addElements() {
    const card = document.getElementById(this.postId);
    const btnsDiv = document.createElement("div");
    btnsDiv.classList.add("btns-div");

    const deleteBtn = document.createElement("button");
    deleteBtn.classList.add("delete-btn");
    deleteBtn.textContent = "Delete card";
    deleteBtn.addEventListener("click", () => this.deleteCard());
    btnsDiv.appendChild(deleteBtn);

    const editBtn = document.createElement("button");
    editBtn.classList.add("edit-btn");
    editBtn.textContent = "Edit card";
    editBtn.addEventListener("click", () => this.editCard());
    btnsDiv.appendChild(editBtn);

    const inputDiv = document.createElement("div");
    inputDiv.classList.add("input-div");

    const changeTitleInput = document.createElement("input");
    changeTitleInput.classList.add("change-title");
    changeTitleInput.type = "text";
    changeTitleInput.placeholder = "Change TITLE";
    inputDiv.appendChild(changeTitleInput);

    const changeTextInput = document.createElement("input");
    changeTextInput.classList.add("change-text");
    changeTextInput.type = "text";
    changeTextInput.placeholder = "Change TEXT";
    inputDiv.appendChild(changeTextInput);

    btnsDiv.appendChild(inputDiv);
    card.appendChild(btnsDiv);
  }
  addCard() {
    const cardsContainer = document.querySelector(".cards-container");
    const cardItem = this.cardTemplate();
    cardsContainer.insertAdjacentHTML("afterbegin", cardItem);
    this.addElements()
  }

  deleteCard() {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
      method: "DELETE"
    })
      .then(response => {
        if (response.ok) {
          const card = document.getElementById(this.postId);
          card.remove();
          alert(`CARD ID "${this.postId}" was successfully removed`);
        }
        else {
          throw new Error(response.status);
        }
      })
      .catch(error => console.error(error));
  }

  editCard() {
    const card = document.getElementById(this.postId);
    const newTitle = card.querySelector(".change-title").value;
    const newBody = card.querySelector(".change-text").value;
    if (!newTitle || !newBody) {
      alert("To EDIT the CARD you have to fill fields: TITLE and TEXT ");
      return;
    }

    fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        title: newTitle,
        body: newBody
      })
    })
      .then(response => checkStatusResponse(response))
      .then(data => {
        console.log(data);
        this.title = newTitle;
        this.body = newBody;
        const cardTitle = card.querySelector(".post-title");
        const cardBody = card.querySelector(".post-body");
        cardTitle.textContent = newTitle;
        cardBody.textContent = newBody;
        alert(`CARD ID "${this.postId}" was successfully edited with NEW TITLE : "${newTitle}" and NEW TEXT : "${newBody}"`);
      })
      .catch(error => console.error(error));
  }
}
function setupModal() {
  const modal = document.querySelector(".modal");
  const overlay = document.querySelector(".overlay");
  const closeModalBtn = document.querySelector(".btn-close");
  const openModalBtn = document.querySelector(".btn-open");
  const submitBtn = document.querySelector(".submit");
  const closeModal = function () {
    modal.classList.add("hidden");
    overlay.classList.add("hidden");
  };

  closeModalBtn.addEventListener("click", closeModal);
  overlay.addEventListener("click", closeModal);

  document.addEventListener("keydown", function (e) {
    if (e.key === "Escape" && !modal.classList.contains("hidden")) {
      closeModal();
    }
  });
  const openModal = function () {
    modal.classList.remove("hidden");
    overlay.classList.remove("hidden");
  };
  openModalBtn.addEventListener("click", openModal);
  submitBtn.addEventListener("click", addNewPost)
};

function addNewPost(e) {
  e.preventDefault();
  const inputTitle = document.getElementById("modal-title");
  const inputBody = document.getElementById("modal-text");
  const newTitle = inputTitle.value;
  const newBody = inputBody.value;

  if (!newBody || !newTitle) {
    alert(`To ADD new post you have to fill fields: "TITLE" and "TEXT" `)
    return;
  }

  fetch(`https://ajax.test-danit.com/api/json/posts/`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      title: newTitle,
      body: newBody
    })
  })
    .then(response => checkStatusResponse(response))
    .then(data => {
      const card = new Card(data.title, data.body, "guest", "guest@example.com", data.id);
      card.addCard(card);
      alert("Card was successful added");
    })
    .catch(error => {
      console.log(error);
    });

  inputTitle.value = '';
  inputBody.value = '';
}

setupModal()